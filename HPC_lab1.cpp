#include <iostream>
#include <algorithm>
#include <time.h>
#include <stdlib.h>
#include <omp.h>
#include <string>
#include <fstream>
#include <ctime>
#include <list>

using namespace std;

ofstream RESULTS_FILE("results.CSV");

void free_array(double** matrix, long int n)
{
	for (int i = 0; i < n; i++)
		delete[] matrix[i];
	delete[] matrix;
}

class Matrix
{
public:
	int N, M;
	double** arr;
	~Matrix() {
		free_array(arr, N);
	}
};

double** malloc_array(long int n, long int m)
{
	double** matrix = new double*[n];
	for (int i = 0; i < n; i++)
		matrix[i] = new double[m];
	return matrix;
}

void rand_init_matrix(double** matrix, long int n, long int m)
{
	srand(time(NULL));
	for (int i = 0; i < n; i++)
		for (int j = 0; j < m; j++)
			matrix[i][j] = rand() % RAND_MAX;
}

Matrix* generate_rand_matrix(long int n, long int m)
{
	double** arr = malloc_array(n, m);
	rand_init_matrix(arr, n, m);

	Matrix* mat = new Matrix();
	mat->arr = arr;
	mat->N = n;
	mat->M = m;

	return mat;
}

void zero_init_matrix(double** matrix, long int n, long int m)
{
	for (int i = 0; i < n; i++)
		for (int j = 0; j < m; j++)
			matrix[i][j] = 0.0;
}

Matrix* generate_zero_matrix(long int n, long int m)
{
	double** arr = malloc_array(n, m);
	zero_init_matrix(arr, n, m);

	Matrix* matr = new Matrix();
	matr->N = n;
	matr->M = m;
	matr->arr = arr;
	return matr;
}

int get_num_threads()
{
	int num_threads;
#pragma omp parallel 
	{
#pragma omp master
		{
			num_threads = omp_get_num_threads();
		}
	}
	return num_threads;
}


class MultiThreadMatrixMultiplyExperiment
{
public:
	Matrix *A, *B;
	int chunk_size;

	MultiThreadMatrixMultiplyExperiment(Matrix *A1, Matrix *B1, int chunk_size1)
	{
		A = A1;
		B = B1;
		chunk_size = chunk_size1;
	}

	void processExperiment()
	{
		double executionTime;

		cout << getExperimentName() << " ";

		executionTime = getAverageExecutionTime(3);

		cout << " time: " << executionTime;
		cout << " A(" << A->N << ", " << A->M << ") and B(" << B->N << ", " << B->M << ")";
		cout << endl;

		string executionTimeStr = to_string(executionTime);
		replace(executionTimeStr.begin(), executionTimeStr.end(), '.', ','); //excel likes comma

		RESULTS_FILE << getExperimentName() << ";";
		RESULTS_FILE << A->N << ";";
		RESULTS_FILE << A->M << ";";
		RESULTS_FILE << get_num_threads() << ";";
		RESULTS_FILE << chunk_size << ";";
		RESULTS_FILE << executionTimeStr << ";";
		RESULTS_FILE << endl;

	}

protected:
	Matrix *C;
	virtual string getExperimentName() { return "experiment without name"; };
	virtual void mulltiply() = 0;

private:
	double getAverageExecutionTime(int experimentCount)
	{
		int i;
		double t, executionTime = 0;
		for (i = experimentCount; i > 0; i--) {
			init_C();
			t = omp_get_wtime();
			mulltiply();
			executionTime += (omp_get_wtime() - t) / experimentCount;
			delete C;
		}
		return executionTime;
	}

	void init_C()
	{
		C = generate_zero_matrix(A->N, B->M);
	}
};


class NoParallel : public MultiThreadMatrixMultiplyExperiment
{
public:
	NoParallel(Matrix *A1, Matrix *B1) : MultiThreadMatrixMultiplyExperiment(A1, B1, -1) {}

protected:
	virtual string getExperimentName() { return "NoParallel"; };

	void mulltiply()
	{
		for (int i = 0; i < A->N; i++)
			for (int k = 0; k < A->M; k++)
				for (int j = 0; j < B->M; j++)
					C->arr[i][j] += A->arr[i][k] * B->arr[k][j];
	}
};

class Static : public MultiThreadMatrixMultiplyExperiment
{
public:
	int size;
	Static(Matrix *A1, Matrix *B1, int size1) : MultiThreadMatrixMultiplyExperiment(A1, B1, size1)
	{
		size = size1;
	}

protected:
	virtual string getExperimentName() { return "Static"; };

	void mulltiply()
	{
#pragma omp parallel for schedule (static, size)
		for (int ij = 0; ij < A->N * B->M; ij++) {
			int i = ij / A->N;
			int j = ij % B->M;
			double tmp = 0.0;
			for (int k = 0; k < A->M; k++)
				tmp += A->arr[i][k] * B->arr[k][j];
			C->arr[i][j] = tmp;
		}
	}
};

class Dynamic : public MultiThreadMatrixMultiplyExperiment
{
public:
	int size;
	Dynamic(Matrix *A1, Matrix *B1, int size1) : MultiThreadMatrixMultiplyExperiment(A1, B1, size1)
	{
		size = size1;
	}

protected:
	virtual string getExperimentName() { return "Dynamic"; };

	void mulltiply()
	{
#pragma omp parallel for schedule (dynamic, size)
		for (int ij = 0; ij < A->N * B->M; ij++) {
			int i = ij / A->N;
			int j = ij % B->M;
			double tmp = 0.0;
			for (int k = 0; k < A->M; k++)
				tmp += A->arr[i][k] * B->arr[k][j];
			C->arr[i][j] = tmp;
		}
	}
};

class Guided : public MultiThreadMatrixMultiplyExperiment
{
public:
	int size;
	Guided(Matrix *A1, Matrix *B1, int size1) : MultiThreadMatrixMultiplyExperiment(A1, B1, size1)
	{
		size = size1;
	}

protected:
	virtual string getExperimentName() { return "Guided"; };

	void mulltiply()
	{
#pragma omp parallel for schedule (guided, size)
		for (int ij = 0; ij < A->N * B->M; ij++) {
			int i = ij / A->N;
			int j = ij % B->M;
			double tmp = 0.0;
			for (int k = 0; k < A->M; k++)
				tmp += A->arr[i][k] * B->arr[k][j];
			C->arr[i][j] = tmp;
		}
	}
};

void processExperiments(Matrix* A, Matrix* B) {
	list<MultiThreadMatrixMultiplyExperiment*> experiments;

	experiments.push_back(new NoParallel(A, B));

	int iterations_matrix_multiply_outer_cycle = A->N * B->M;

	int small_chunk_size = 1;
	experiments.push_back(new Static(A, B, small_chunk_size));
	experiments.push_back(new Dynamic(A, B, small_chunk_size));
	experiments.push_back(new Guided(A, B, small_chunk_size));

	if (A->N == 1000 && get_num_threads() == 4) {
		/* change chunk_size for PICTURE 3 */

		int normal_chunk_size = iterations_matrix_multiply_outer_cycle / 10;
		experiments.push_back(new Static(A, B, normal_chunk_size));
		experiments.push_back(new Dynamic(A, B, normal_chunk_size));
		experiments.push_back(new Guided(A, B, normal_chunk_size));

		int perfect_chunk_size = iterations_matrix_multiply_outer_cycle / get_num_threads();
		experiments.push_back(new Static(A, B, perfect_chunk_size));
		experiments.push_back(new Dynamic(A, B, perfect_chunk_size));
		experiments.push_back(new Guided(A, B, perfect_chunk_size));

		int big_chunk_size = iterations_matrix_multiply_outer_cycle;
		experiments.push_back(new Static(A, B, big_chunk_size));
		experiments.push_back(new Dynamic(A, B, big_chunk_size));
		experiments.push_back(new Guided(A, B, big_chunk_size));
	}

	for (MultiThreadMatrixMultiplyExperiment* var : experiments)
	{
		var->processExperiment();
		delete var;
	}
}

void processExperiments(int N, int M) {
	Matrix* A = generate_rand_matrix(N, M);
	Matrix* B = generate_rand_matrix(M, N);
	cout << "Matrixes are generated, start experiments" << endl;

	processExperiments(A, B);

	delete A;
	delete B;
}

void processExperiments(int N) {
	processExperiments(N, N);
}

void printElapsedTime(clock_t start) {
	cout << "Time: " << (double)(clock() - start) / CLOCKS_PER_SEC << endl;
}

int main() {
	omp_set_nested(true);

	RESULTS_FILE << "Name;N;M;Number of threads;Chunk size;Execution time;" << endl;
	clock_t start = clock();

	for (int num_threads = 2; num_threads <= 4; num_threads++) {
		omp_set_num_threads(num_threads);
		if (num_threads == 4)
		{
			/* change matrix_size for FIGURE 1 */
			for (int matrix_size = 100; matrix_size <= 1000; matrix_size += 100)
			{
				processExperiments(matrix_size);
				printElapsedTime(start);
			}
		}
		else
		{
			/* FIGURE 2 */
			processExperiments(1000);
			printElapsedTime(start);
		}
	}

	cout << "End program: " << endl;
	printElapsedTime(start);
	RESULTS_FILE.close();
	return 0;
}